const axios = require('axios');
const Eckles = require('eckles');

const { buildGetter } = require('./utils');

const defaultAlgorithms = ['ES256'];

class KeyNotAvailableError extends Error {}

class MultipleKeysError extends Error {}

const getMemberProfilesProjectIdPrefix = ({ env = process.env } = {}) => (
  env.member_profiles_project_id_prefix
);
const getMemberProfilesUrl = (opts = {}) => {
  const env = opts.env || process.env;
  if (env.member_profiles_url) return env.member_profiles_url;
  const prefix = buildGetter(getMemberProfilesProjectIdPrefix)(opts);
  return `https://australia-southeast1-${prefix}-dev.cloudfunctions.net`;
};
const getJwkFromEnv = ({ env = process.env } = {}) => (
  env.member_profiles_jwk_b64
    ? JSON.parse(Buffer.from(env.member_profiles_jwk_b64, 'base64').toString())
    : {}
);
const getJwkFromHttp = async (opts = {}) => {
  const url = buildGetter(getMemberProfilesUrl)(opts);
  const resp = await axios.get('/jwks', { baseURL: url });
  if (resp.data.keys.length === 0) {
    return null;
  } if (resp.data.keys.length > 1) {
    throw new MultipleKeysError();
  }
  return resp.data.keys[0];
};
const getJwk = async (opts = {}) => {
  const keys = buildGetter(getJwkFromEnv)(opts);
  if (keys[opts.key]) return keys[opts.key];

  throw new KeyNotAvailableError(`${opts.key} key unavailable`);
};
const getJwkPublic = async (opts = {}) => {
  try {
    return await getJwk({ ...opts, key: 'public' });
  } catch (err) {
    if (!(err instanceof KeyNotAvailableError)) throw err;
  }

  const httpKey = await buildGetter(getJwkFromHttp)(opts);
  if (httpKey) return httpKey;

  throw new KeyNotAvailableError('public key unavailable');
};
const getJwkPrivate = (opts = {}) => getJwk({ ...opts, key: 'private' });
const buildPemGetter = jwkGetter => async (opts = {}) => {
  const jwk = await buildGetter(jwkGetter)(opts);
  return Eckles.export({ jwk });
};
const getPemPublic = buildPemGetter(getJwkPublic);
const getPemPrivate = buildPemGetter(getJwkPrivate);
const getJwtAlgorithms = async (opts = {}) => {
  try {
    const key = await buildGetter(getJwkPublic)(opts);
    if (key.alg) return [key.alg];
  } catch (err) {
    if (!(err instanceof KeyNotAvailableError)) throw err;
  }
  return opts.defaultAlgorithms || defaultAlgorithms;
};

module.exports = {
  di: {
    getMemberProfilesProjectIdPrefix,
    getMemberProfilesUrl,
    getJwkFromEnv,
    getJwkFromHttp,
    getJwkPublic,
    getJwkPrivate,
    getPemPublic,
    getPemPrivate,
    getJwtAlgorithms,
  },
  KeyNotAvailableError,
  MultipleKeysError,
  defaultAlgorithms,
};
