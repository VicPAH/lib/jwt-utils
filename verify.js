const _ = require('lodash');
const jwt = require('jsonwebtoken');

const { di: { getJwtAlgorithms, getPemPublic } } = require('./get');
const { buildGetter } = require('./utils');

const verifyJwt = async (opts = {}) => {
  let token;
  let verifyOpts = {};
  if (_.isPlainObject(opts)) {
    token = opts.token;
    verifyOpts = _.omit(opts, ['token']);
  } else {
    token = opts;
  }

  const [pem, algorithms] = await Promise.all([
    buildGetter(getPemPublic)(opts),
    buildGetter(getJwtAlgorithms)(opts),
  ]);

  return jwt.verify(token, pem, { ...verifyOpts, algorithms });
};

module.exports = {
  di: {
    verifyJwt,
  },
};
