// Builds a function that either gets an override from
// opts, or uses the default getter
exports.buildGetter = jwkGetter => (opts = {}) => {
  const getter = opts[jwkGetter.name] || jwkGetter;
  return getter(opts);
};
