const jwt = require('jsonwebtoken');

const { di } = require('./verify');
const { createKeys } = require('./test_utils');

let testJwks;

beforeEach(async () => {
  testJwks = await createKeys();
});

const testAlg = 'ES256';
const getJwkPublic = () => ({ ...testJwks.public, alg: testAlg });

describe.only('verifyJwt()', () => {
  test('valid', async () => {
    expect.assertions(1);
    const payload = { test: 'payload test value' };
    const alg = 'ES256';
    const aud = 'opt audience value';
    const token = jwt.sign(
      payload,
      testJwks.privatePem,
      { algorithm: alg, audience: aud },
    );
    await expect(di.verifyJwt({
      token, getJwkPublic,
    }))
      .resolves
      .toEqual(expect.objectContaining({
        ...payload,
        iat: expect.any(Number),
        aud,
      }));
  });
  test('broken', async () => {
    expect.assertions(1);
    const token = 'not real';
    await expect(di.verifyJwt({
      token, getJwkPublic,
    }))
      .rejects
      .toThrowError(/jwt malformed/);
  });
  test('incorrect key', async () => {
    expect.assertions(1);
    const keys = await createKeys();
    const token = await jwt.sign(
      {}, keys.privatePem, { algorithm: testAlg },
    );
    await expect(di.verifyJwt({
      token, getJwkPublic,
    }))
      .rejects
      .toThrowError(/invalid signature/);
  });
  test('incorrect alg', async () => {
    expect.assertions(1);
    const token = await jwt.sign(
      {}, testJwks.privatePem, { algorithm: 'HS256' },
    );
    await expect(di.verifyJwt({
      token, getJwkPublic,
    }))
      .rejects
      .toThrowError(/invalid algorithm/);
  });
  test('expired', async () => {
    expect.assertions(1);
    const token = await jwt.sign(
      {}, testJwks.privatePem, { algorithm: testAlg, expiresIn: 0 },
    );
    await expect(di.verifyJwt({
      token, getJwkPublic,
    }))
      .rejects
      .toThrowError(/jwt expired/);
  });
  test('nbf', async () => {
    expect.assertions(1);
    const token = await jwt.sign(
      {}, testJwks.privatePem, { algorithm: testAlg, notBefore: 10 },
    );
    await expect(di.verifyJwt({
      token, getJwkPublic,
    }))
      .rejects
      .toThrowError(/jwt not active/);
  });
  describe('audience', () => {
    test('accepts correct exact match', async () => {
      expect.assertions(1);
      const token = await jwt.sign(
        {}, testJwks.privatePem, { algorithm: testAlg, audience: ['v.o.a/r'] },
      );
      await expect(di.verifyJwt({
        token,
        getJwkPublic,
        audience: 'v.o.a/r',
      }))
        .resolves
        .toEqual(expect.objectContaining({
          aud: ['v.o.a/r'],
        }));
    });
    test('rejects incorrect exact match', async () => {
      expect.assertions(1);
      const token = await jwt.sign(
        {}, testJwks.privatePem, { algorithm: testAlg, audience: ['v.o.a/other'] },
      );
      await expect(di.verifyJwt({
        token,
        getJwkPublic,
        audience: 'v.o.a/r',
      }))
        .rejects
        .toThrowError(/jwt audience invalid/);
    });
  });
});
