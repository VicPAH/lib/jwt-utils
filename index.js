const create = require('./create');
const get = require('./get');
const verify = require('./verify');

module.exports = {
  ...create,
  ...get,
  ...verify,
  di: {
    ...create.di,
    ...get.di,
    ...verify.di,
  },
};
