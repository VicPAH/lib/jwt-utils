const Eckles = require('eckles');

exports.profilesEnvKey = 'member_profiles_project_id_prefix';
exports.profilesUrlEnvKey = 'member_profiles_url';
exports.jwkEnvKey = 'member_profiles_jwk_b64';

const keyToEnv = data => Buffer.from(JSON.stringify(data)).toString('base64');
exports.keyToEnv = keyToEnv;
exports.createKeys = async () => {
  const jwk = await Eckles.generate({ format: 'jwk' });
  return {
    ...jwk,
    privatePem: await Eckles.export({ jwk: jwk.private }),
    publicPem: await Eckles.export({ jwk: jwk.public }),
    bothB64: keyToEnv(jwk),
    privateB64: keyToEnv({ private: jwk.private }),
    publicB64: keyToEnv({ public: jwk.public }),
  };
};
