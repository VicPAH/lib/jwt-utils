const axios = require('axios');
const MockAdapter = require('axios-mock-adapter');
const _ = require('lodash');

const { defaultAlgorithms, di, KeyNotAvailableError, MultipleKeysError } = require('./get');
const {
  createKeys, jwkEnvKey, keyToEnv, profilesEnvKey, profilesUrlEnvKey,
} = require('./test_utils');

const axiosMock = new MockAdapter(axios);

let testJwks;
const origProfilesEnvVal = process.env[profilesEnvKey];
const origProfilesUrlEnvVal = process.env[profilesUrlEnvKey];
const origJwkEnvVal = process.env[jwkEnvKey];

beforeEach(async () => {
  axiosMock.reset();
  testJwks = await createKeys();

  // Clear env vars
  delete process.env[jwkEnvKey];
  delete process.env[profilesEnvKey];
  delete process.env[profilesUrlEnvKey];
});
afterAll(() => {
  axiosMock.restore();

  // Restore env vars
  if (origJwkEnvVal) process.env[jwkEnvKey] = origJwkEnvVal;
  if (origProfilesEnvVal) process.env[profilesEnvKey] = origProfilesEnvVal;
  if (origProfilesUrlEnvVal) process.env[profilesUrlEnvKey] = origProfilesUrlEnvVal;
});

describe('getMemberProfilesProjectIdPrefix()', () => {
  const key = profilesEnvKey;
  const value = new Date().toString();

  test(`uses ${key} from arg`, () => {
    expect(di.getMemberProfilesProjectIdPrefix({
      env: { [key]: value },
    }))
      .toBe(value);
  });
  test(`uses ${key} from process.env`, () => {
    process.env[key] = value;
    expect(di.getMemberProfilesProjectIdPrefix())
      .toBe(value);
  });
});

describe('getMemberProfilesUrl()', () => {
  // TODO environ (dev/prod)
  test('https, region, project id prefix, env, and domain', () => {
    const prefix = 'this-is-the-test-project';
    expect(di.getMemberProfilesUrl({
      getMemberProfilesProjectIdPrefix: () => prefix,
    }))
      .toEqual(`https://australia-southeast1-${prefix}-dev.cloudfunctions.net`);
  });
  test('override with member_profiles_url', () => {
    process.env[profilesUrlEnvKey] = 'http://different:8080/test';
    expect(di.getMemberProfilesUrl())
      .toEqual('http://different:8080/test');
  });
});

describe('getJwkFromEnv()', () => {
  const key = jwkEnvKey;
  const rawVal = { public: 'test public value', private: 'test private value' };
  const envVal = Buffer.from(JSON.stringify(rawVal)).toString('base64');
  test(`parses ${key} from arg`, () => {
    expect(di.getJwkFromEnv({
      env: { [key]: envVal },
    }))
      .toEqual(rawVal);
  });
  test(`parses ${key} from process.env`, () => {
    process.env[key] = envVal;
    expect(di.getJwkFromEnv())
      .toEqual(rawVal);
  });
  test("doesn't error when not given", () => {
    expect(() => di.getJwkFromEnv({
      env: {},
    }))
      .not
      .toThrowError();
  });
});

describe('getJwkFromHttp()', () => {
  const url = 'http://test';
  const opts = {
    getMemberProfilesUrl: () => url,
  };
  test('returns the key on 1 key', async () => {
    expect.assertions(1);
    const val = 'theval';
    axiosMock.onGet('/jwks').reply(200, { keys: [val] });
    await expect(di.getJwkFromHttp(opts))
      .resolves
      .toBe(val);
  });
  test('null on no keys', async () => {
    expect.assertions(1);
    axiosMock.onGet('/jwks').reply(200, { keys: [] });
    await expect(di.getJwkFromHttp(opts))
      .resolves
      .toBe(null);
  });
  test('errors on multiple keys', async () => {
    expect.assertions(1);
    axiosMock.onGet('/jwks').reply(200, { keys: [1, 2] });
    await expect(di.getJwkFromHttp(opts))
      .rejects
      .toThrowError(MultipleKeysError);
  });
});

describe('getJwkPrivate()', () => {
  test('throws error when not in env', async () => {
    expect.assertions(1);
    axiosMock.onAny(/.*/).networkError();
    await expect(di.getJwkPrivate({
      env: {},
    }))
      .rejects
      .toThrowError(KeyNotAvailableError);
  });
  test('returns private from getJwkFromEnv()', async () => {
    expect.assertions(1);
    const sym = Symbol('private jwk');
    axiosMock.onAny(/.*/).networkError();
    await expect(di.getJwkPrivate({
      getJwkFromEnv: () => ({ private: sym }),
    }))
      .resolves
      .toBe(sym);
  });
});

describe('getJwkPublic()', () => {
  test('throws error when not in env or http', async () => {
    expect.assertions(1);
    axiosMock.onGet('/jwks').reply(200, { keys: [] });
    await expect(di.getJwkPublic({
      env: {},
    }))
      .rejects
      .toThrowError(KeyNotAvailableError);
  });
  test('returns public from getJwkFromEnv()', async () => {
    expect.assertions(1);
    const sym = Symbol('public jwk');
    axiosMock.onAny(/.*/).networkError();
    await expect(di.getJwkPublic({
      getJwkFromEnv: () => ({ public: sym }),
    }))
      .resolves
      .toBe(sym);
  });
  test('returns public from getJwkFromHttp()', async () => {
    expect.assertions(1);
    const sym = Symbol('public jwk');
    await expect(di.getJwkPublic({
      getJwkFromEnv: () => ({}),
      getJwkFromHttp: () => Promise.resolve(sym),
    }))
      .resolves
      .toBe(sym);
  });
  test('bubbles MultipleKeysError from getJwkFromHttp()', async () => {
    expect.assertions(1);
    const err = new MultipleKeysError();
    await expect(di.getJwkPublic({
      getJwkFromEnv: () => ({}),
      getJwkFromHttp: () => Promise.reject(err),
    }))
      .rejects
      .toBe(err);
  });
});

describe('getPemPrivate()', () => {
  test('throws error when not in env', async () => {
    expect.assertions(1);
    axiosMock.onAny(/.*/).networkError();
    await expect(di.getPemPrivate({
      env: {},
    }))
      .rejects
      .toThrowError(KeyNotAvailableError);
  });
  test('correct PEM when key in env', async () => {
    expect.assertions(1);
    axiosMock.onAny(/.*/).networkError();
    await expect(di.getPemPrivate({
      env: { [jwkEnvKey]: testJwks.bothB64 },
    }))
      .resolves
      .toEqual(testJwks.privatePem);
  });
  test('callable without opts', async () => {
    expect.assertions(1);
    await expect(di.getPemPrivate())
      .rejects
      .toThrowError(KeyNotAvailableError);
  });
});

describe('getPemPublic()', () => {
  test('throws error when not in env or http', async () => {
    expect.assertions(1);
    axiosMock.onGet('/jwks').reply(200, { keys: [] });
    await expect(di.getPemPublic({
      env: {},
    }))
      .rejects
      .toThrowError(KeyNotAvailableError);
  });
  test('correct PEM when key in env', async () => {
    expect.assertions(1);
    axiosMock.onAny(/.*/).networkError();
    await expect(di.getPemPublic({
      env: { [jwkEnvKey]: testJwks.bothB64 },
    }))
      .resolves
      .toEqual(testJwks.publicPem);
  });
  test('correct PEM when key from HTTP', async () => {
    expect.assertions(1);
    axiosMock.onGet('/jwks').reply(200, { keys: [testJwks.public] });
    await expect(di.getPemPublic({
      env: {},
    }))
      .resolves
      .toEqual(testJwks.publicPem);
  });
  test('callable without opts', async () => {
    expect.assertions(1);
    axiosMock.onGet('/jwks').reply(200, { keys: [] });
    await expect(di.getPemPublic())
      .rejects
      .toThrowError(KeyNotAvailableError);
  });
});

describe('getJwtAlgorithms()', () => {
  test('uses alg from public key', async () => {
    expect.assertions(1);
    axiosMock.onAny(/.*/).networkError();
    await expect(di.getJwtAlgorithms({
      env: {
        [jwkEnvKey]: keyToEnv({
          public: {
            ...testJwks.public,
            alg: 'this is a test',
          },
        }),
      },
    }))
      .resolves
      .toEqual(['this is a test']);
  });
  test('no error, no undef when public key doesnt include alg', async () => {
    expect.assertions(1);
    axiosMock.onAny(/.*/).networkError();
    await expect(di.getJwtAlgorithms({
      env: {
        [jwkEnvKey]: keyToEnv({
          public: _.omit(testJwks.public, ['alg']),
        }),
      },
    }))
      .resolves
      .toEqual([expect.anything()]);
  });
  test('uses default algorithms from opts', async () => {
    const testDefaultAlgorithms = [Symbol('test'), Symbol('another')];
    expect.assertions(1);
    axiosMock.onGet('/jwks').reply(200, { keys: [] });
    await expect(di.getJwtAlgorithms({
      env: {},
      defaultAlgorithms: testDefaultAlgorithms,
    }))
      .resolves
      .toBe(testDefaultAlgorithms);
  });
  test('uses static default algorithms', async () => {
    expect.assertions(1);
    axiosMock.onGet('/jwks').reply(200, { keys: [] });
    await expect(di.getJwtAlgorithms({
      env: {},
    }))
      .resolves
      .toBe(defaultAlgorithms);
  });
  test('bubbles MultipleKeysError', async () => {
    expect.assertions(1);
    await expect(di.getJwtAlgorithms({
      getJwkPublic: () => { throw new MultipleKeysError(); },
    }))
      .rejects
      .toThrowError(MultipleKeysError);
  });
  test('callable without opts', async () => {
    expect.assertions(1);
    axiosMock.onGet('/jwks').reply(200, { keys: [{ alg: 'test' }] });
    await expect(di.getJwtAlgorithms())
      .resolves
      .toEqual(['test']);
  });
});
